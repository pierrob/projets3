"test" : main.o Utilitaires.o Briques.o Barre.o Balle.o
	gcc -o "test" main.o Utilitaires.o Briques.o Barre.o Balle.o -L/usr/local/lib -lSDL2 -lSDL2_image
main.o: main.c Globale.h
	gcc -c main.c Globale.h $(pkg-config --libs sdl2) -lSDL2 -lSDL2_image
Utilitaires.o: Utilitaires.c Globale.h
	gcc -c Utilitaires.c Globale.h $(pkg-config --libs sdl2) -lSDL2 -lSDL2_image
Briques.o: Briques.c Briques.h Globale.h
	gcc -c Briques.c Briques.h Globale.h $(pkg-config --libs sdl2) -lSDL2 -lSDL2_image
Barre.o: Barre.c Barre.h Globale.h
	gcc -c Barre.c Barre.h Globale.h $(pkg-config --libs sdl2) -lSDL2 -lSDL2_image
Balle.o: Balle.c Balle.h
	gcc -c Balle.c Balle.h $(pkg-config --libs sdl2) -lSDL2 -lSDL2_image

	@echo "ok"
