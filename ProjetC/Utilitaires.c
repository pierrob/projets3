//
//  Utilitaires.c
//  ProjetC
//
//  Created by Pierre Bonijol on 29/11/2017.
//  Copyright © 2017 Pierre Bonijol. All rights reserved.
//

#include "Globale.h"
#include <SDL2_ttf/SDL_ttf.h>
SDL_Window* window;
SDL_Renderer* renderer;

SDL_Texture* textureBg;
SDL_Texture* textureBrique;
SDL_Texture* textureBarre;
SDL_Texture* textureBalle;

SDL_Surface* surfaceBrique;
SDL_Surface* surfaceBg;
SDL_Surface* surfaceBarre;
SDL_Surface* surfaceBalle;

int nb_brique_colonne;
int nb_brique_ligne;
int wbrique;
int hbrique;
int xbrique;
int ybrique;
int tir = 0;
int score = 0;

Barre barre;
Balle balle;

TTF_Font* Sans;
SDL_Color White= {255, 255, 255};

void Generer(SDL_Texture* nom,int x, int y, int w, int h, SDL_Rect pos) //permet de generer un positionnement pour la texture et de la copier dans le renderer
{
    pos.x = x;
    pos.y = y;
    pos.w = w;
    pos.h = h;
    SDL_RenderCopy(renderer, nom, NULL, &pos);
}
void charger_images()
{
    IMG_Init(IMG_INIT_JPG); //l'image qui va etre chargée est en format jpg
    surfaceBg = IMG_Load("Images/background.jpg");
    IMG_Quit();
    
    IMG_Init(IMG_INIT_PNG); //les images qui vont etres chargées sont en format png
    surfaceBarre = IMG_Load("Images/barre.png");
    surfaceBrique = IMG_Load("Images/brique.png");
    surfaceBalle = IMG_Load("Images/ball.png");
    IMG_Quit();
}
int initialisation()
{
    window = SDL_CreateWindow("Casse Briques",
                              SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                              largeurfenetre, hauteurfenetre, SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL); //initialisation de la fenêtre
    if (!window) {
        return 0;
    }
    
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);    //initialisation du rendu
    if (!renderer) {
        return 0;
    }
    
    Sans = TTF_OpenFont("maske.ttf", 100); //this opens a font style and sets a size
    
    
    barre.pos.x = (largeurfenetre / 2) - 40;
    barre.pos.y = 455;
    barre.pos.w = 100;
    barre.pos.h = 30;                              //on initilise les variables
    
    balle.pos.y = 445;
    balle.pos.w = 15;
    balle.pos.h = 15;
    balle.xvel = 5;
    balle.yvel= -5;
    
    wbrique = 50;
    hbrique = 30;
    
    xbrique = 4;
    ybrique = 30;
    
    nb_brique_ligne = 4;
    nb_brique_colonne = 19;
    
    return 1;
}
int collides(SDL_Rect a, SDL_Rect b) {
    // the side of the rects
    int a_left, a_right, b_left, b_right;
    int a_top, a_bottom, b_top, b_bottom;
    
    a_left = a.x;
    a_right = a.x + a.w;
    a_top = a.y;
    a_bottom = a.y + a.h;
    
    b_left = b.x;
    b_right = b.x + b.w;
    b_top = b.y;
    b_bottom = b.y + b.h;
    
    if (a_bottom <= b_top) return 0;
    if (a_top >= b_bottom) return 0;
    if (a_right <= b_left) return 0;
    if (a_left >= b_right) return 0;
    
    return 1;
}
int CheckCollisionBriqueBalle(SDL_Rect rect1, SDL_Rect rect2)
{
    int rect1_left  = rect1.x;
    int rect1_right = rect1.x + rect1.w;
    int rect1_up    = rect1.y;
    int rect1_down  = rect1.y + rect1.h;
    int rect2_left  = rect2.x;
    int rect2_right = rect2.x + rect2.w;
    int rect2_up    = rect2.y;
    int rect2_down  = rect2.y + rect2.h;
    
    if( (rect1_down >= rect2_up) && (rect1_up <= rect2_up) && (rect1_right >= rect2_left) && (rect1_left <= rect2_right) )
        return 1;// haut
    if( (rect1_left <= rect2_right) && (rect1_right >= rect2_right) && (rect1_down >= rect2_up) && (rect1_up <= rect2_down) )
        return 2;//droite
    if( (rect1_up <= rect2_down) && (rect1_up >= rect2_up) && (rect1_right >= rect2_left) && (rect1_left <= rect2_right) )
        return 3;//bas
    if( (rect1_right >= rect2_left) && (rect1_left <= rect2_left) && (rect1_down >= rect2_up) && (rect1_up <= rect2_down) )
        return 4;//gauche
    return 0;
    
}
unsigned int lasttick;
void charger_textures()
{
        lasttick = SDL_GetTicks();
    textureBg = SDL_CreateTextureFromSurface(renderer, surfaceBg); // initialisation de la texture
    
    textureBarre = SDL_CreateTextureFromSurface(renderer, surfaceBarre);
    
    textureBrique = SDL_CreateTextureFromSurface(renderer, surfaceBrique);
    
    textureBalle = SDL_CreateTextureFromSurface(renderer, surfaceBalle);
    
}
void DrawScore()
{


    
    char buf[20];
    sprintf(buf, "Score : %d", score);
    SDL_Surface* surfaceMessage = TTF_RenderText_Solid(Sans, buf, White); // as TTF_RenderText_Solid could only be used on SDL_Surface then you have to create the surface first
    
    SDL_Texture* Message = SDL_CreateTextureFromSurface(renderer, surfaceMessage); //now you can convert it into a texture
    
    SDL_Rect Message_rect; //create a rect
    Message_rect.x = largeurfenetre - 100;  //controls the rect's x coordinate
    Message_rect.y = -10; // controls the rect's y coordinte
    Message_rect.w = 90; // controls the width of the rect
    Message_rect.h = 50; // controls the height of the rect
    SDL_RenderCopy(renderer, Message, NULL, &Message_rect);
    

}
void Update() //on met a jour le rendu
{
    unsigned int curtick = SDL_GetTicks();
    float delta = (curtick - lasttick) / 1000.0f;
    lasttick = curtick;
    
    SDL_RenderClear(renderer);
    SDL_RenderCopy(renderer, textureBg, NULL, NULL);
    GenererBrique( 19, 4, textureBrique); // voir briques.c
    
    DrawScore();
    
    if(left)
        SetRaquetteX( barre.pos.x - speed);
    
    if(right)
        SetRaquetteX( barre.pos.x + speed);
    
    if (tir) //si on tir alors la balle se decroche de la position x de la barre
        Generer( textureBalle,balle.pos.x, balle.pos.y, balle.pos.w, balle.pos.h, balle.pos);
    else
        Generer( textureBalle,barre.pos.x + barre.pos.w/2 - 5, balle.pos.y, balle.pos.w, balle.pos.h, balle.pos);
    
    Generer(textureBarre, barre.pos.x, barre.pos.y, barre.pos.w, barre.pos.h, barre.pos);
    MouvementBalle();
    checkb();
    SDL_RenderPresent(renderer);
    SDL_Delay(1000/60);
}
void freeimages()
{
    SDL_FreeSurface(surfaceBg);
    SDL_FreeSurface(surfaceBarre);
    SDL_FreeSurface(surfaceBrique);
    SDL_FreeSurface(surfaceBalle);
}
