//
//  Barre.h
//  ProjetC
//
//  Created by Pierre Bonijol on 29/11/2017.
//  Copyright © 2017 Pierre Bonijol. All rights reserved.
//

#ifndef Barre_h
#define Barre_h
#include <SDL2/SDL.h>                   //on inclut le sdl2
#include <SDL2_image/SDL_image.h>

typedef struct _Barre {  //On definit la structure Barre
    SDL_Rect pos;//toutes les nouvelles positions seront gérées ici
}Barre;

extern SDL_Surface* surfaceBarre;
extern SDL_Texture* textureBarre;

extern void SetRaquetteX(int x);

#endif /* Barre_h */
