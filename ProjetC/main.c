//
//  main.c
//  ProjetC
//
//  Created by Pierre Bonijol on 28/11/2017.
//  Copyright © 2017 Pierre Bonijol. All rights reserved.
//


#include "Globale.h"
#include <SDL2_ttf/SDL_ttf.h>


SDL_Event event;

int hauteurfenetre = 600;
int largeurfenetre = 800;
int speed = 10;//vitesse de la barre

int left = 0;
int right = 0;


int main(int argc, const char * argv[]) {
    
    SDL_Init(SDL_INIT_VIDEO); // on initialise le sdl

    TTF_Init();
    
    if(initialisation() != 1) //initialisation des surfaces dans Utilitaires.c
        return 0;

    charger_images(); //chargement des images dans Utilitaires.c

    charger_textures(); //chargement des textures dans Utilitaires.c
   
   // freeimages(); //free des images dans Utilitaires.c
  
    while(event.type != SDL_QUIT)  // Tant que l'event n'est pas Quit, effectuer la boucle
    {
       Update();//mis a jour du rendu dans Utilitaires.c
      
        while(SDL_PollEvent(&event)) // on recupere les evenement du clavier
        {
            
            switch (event.type)
            {
                case SDL_KEYDOWN://si on appuis sur la touche:
                    switch( event.key.keysym.sym ){ // quelle touche est appuyée
                        case SDLK_LEFT: //touche gauche
                            left = 1;
                           // SetRaquetteX( barre.pos.x - speed); // on deplace la barre à gauche celon la vitesse dans Barre.c
                            break;
                        case SDLK_RIGHT:
                            right = 1;
                            // on deplace la barre à droite celon la vitesse dans Barre.c
                            break;
                        case SDLK_SPACE:  //on tir si on appuis sur la barre d'espace
                            tir = 1;
                            break;
                        case SDLK_ESCAPE: // on quitte si on appuit sur echap
                            event.type = SDL_QUIT;
                            break;
                        default:
                            break;
                    }
                    break;
                case SDL_KEYUP:
                    switch(event.key.keysym.sym){
                        case SDLK_LEFT:
                            left = 0;
                            break;
                        case SDLK_RIGHT:
                            right = 0;
                            break;
                    }
                    break;
            }
            }
        }
    }

