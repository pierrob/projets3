//
//  Barre.c
//  ProjetC
//
//  Created by Pierre Bonijol on 29/11/2017.
//  Copyright © 2017 Pierre Bonijol. All rights reserved.
//

#include "Globale.h"


void SetRaquetteX( int x)
{
    if (x <= 0) // si la barre atteind le bord gauche on la place a 0
        barre.pos.x = 0;
    else if (x > largeurfenetre - barre.pos.w) //si la barre atteind le bord droit on l'empeche de depasser
        barre.pos.x = largeurfenetre - barre.pos.w;
    else
        barre.pos.x = x; //sinon on la deplace
}
