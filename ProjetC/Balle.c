//
//  Balle.c
//  ProjetC
//
//  Created by Pierre Bonijol on 30/11/2017.
//  Copyright © 2017 Pierre Bonijol. All rights reserved.
//

#include "Globale.h"
int colisionbt = 0;
int flags = 0;
int barreok = 0;


void checkb()
{
    for(int h = 0;h < nb_brique_ligne; h++)
    {
        for(int i = 0;i < nb_brique_colonne; i++)
        {
            
            if(CheckCollisionBriqueBalle(balle.pos, briques[h][i].pos) == 1 && briques[h][i].detruit == 0)
            {
                
                briques[h][i].detruit = 1;
                score += 10;
                flags = 1;
            }
                
            else if (CheckCollisionBriqueBalle(balle.pos, briques[h][i].pos) == 2 && briques[h][i].detruit == 0)
                {
                    briques[h][i].detruit = 1;
                    score += 10;
                    flags = 2;
                }
            else if (CheckCollisionBriqueBalle(balle.pos, briques[h][i].pos) == 3 && briques[h][i].detruit == 0)
            {
                briques[h][i].detruit = 1;
                score += 10;
                flags = 3;
            }
            else if (CheckCollisionBriqueBalle(balle.pos, briques[h][i].pos) == 4 && briques[h][i].detruit == 0)
            {
                briques[h][i].detruit = 1;
                score += 10;
                flags = 4;
            }
            else
            {
            }
            }
            
        }
       
        
    }
#define BALLSPEED       200


void MouvementBalle()
{

   if(tir == 1) //si on tir et que la balle ne depasse pas le haut de la fenetre alors on lui retire 1 en ordonée en boucle
    {
    
        if(barreok)
             balle.pos.x+=balle.xvel;
            

    balle.pos.y+=balle.yvel;
        
    
        
        if (balle.pos.x < 0) //wall collision - left
        {
            balle.xvel = -balle.xvel;

        }
        
        if (balle.pos.x > largeurfenetre - balle.pos.w) //wall collision - right
        {
            balle.xvel = -balle.xvel;
        }
        if (balle.pos.y < 0) //wall collision - top
        {
            balle.yvel = -balle.yvel;
           
        }
        if (collides(balle.pos,barre.pos) && barreok) //paddle collision - top
        {
            balle.yvel = -balle.yvel;
            
            //change balls x velocity depending on where it hit the paddle
            if (balle.pos.x < barre.pos.x+10)
                balle.xvel = -4;
            else if (balle.pos.x < barre.pos.x+20)
                balle.xvel = -3;
            else if (balle.pos.x < barre.pos.x+30)
                balle.xvel = -2;
            else if (balle.pos.x < barre.pos.x+40)
                balle.xvel = -1;
            else if (balle.pos.x < barre.pos.x+50)
                balle.xvel = 0;
            else if (balle.pos.x < barre.pos.x+60)
                balle.xvel = 1;
            else if (balle.pos.x < barre.pos.x+80)
                balle.xvel = 2;
            else if (balle.pos.x < barre.pos.x+90)
                balle.xvel = 3;
            else if (balle.pos.x < barre.pos.x+100)
                balle.xvel = 4;
        }
        
        if(flags == 1)
        {
            
            balle.yvel = -balle.yvel; //haut
             barreok = 1;
            flags = 0;
        }
        
        if(flags == 2)
        {
            balle.xvel = -balle.xvel; //bord droit

            flags = 0;
        }
        if(flags == 3)
        {
            balle.yvel = -balle.yvel; // bas de la brique
            barreok = 1;
            flags = 0;
        }
        if(flags == 4)
        {
            balle.xvel = -balle.xvel; //bord gauche
            flags = 0;
        }
        if(balle.pos.y>hauteurfenetre)
        {
            SDL_Delay(1000);
            balle.yvel = -balle.yvel;
            barreok = 0;
            tir = 0;
            
        }

        
    }
    else
    {
        tir = 0;
        balle.pos.x = barre.pos.x + barre.pos.w/2 -5; //mis a hour de la derniere position de la balle avant un tir
        balle.pos.y = 445;
        
    
    }
    
}
