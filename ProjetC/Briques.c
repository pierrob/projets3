//
//  Briques.c
//  ProjetC
//
//  Created by Pierre Bonijol on 29/11/2017.
//  Copyright © 2017 Pierre Bonijol. All rights reserved.
//

#include "Briques.h"
#include "Globale.h"

Brique **briques;

int ini = 0;

void Reset()
{
    ybrique += hbrique;
    xbrique = 4;
}
void GenererBrique(int longueur,int hauteur,SDL_Texture* texture) // generer des briques celon la longueur et hauteur choisi, puis rendercopy vers le renderer avec le nom de texture choisi
{
    if (ini == 0) //on initialise une seule fois l'allocation car la fonction va etre appellée plusieurs fois
    {
    briques = (Brique **)malloc(longueur * sizeof(Brique *));
    for (int l = 0; l < hauteur; ++l)
        briques[l] = (Brique *)malloc(longueur * sizeof(Brique)); // on alloue briques a deux dimensions celon la hauteur et largeur choisi
        
    ini = 1;
        
        for(int h = 0;h < hauteur; h++)
        {
            for(int i = 0;i < longueur; i++)
            {
                briques[h][i].detruit = 0;
                
            }
            
        }
    }

    for(int h = 0;h < hauteur; h++)
    {
        for(int i = 0;i < longueur; i++)
        {
            briques[h][i].pos.x = xbrique;
            briques[h][i].pos.y = ybrique; // on stock les positions de chaques briques.
            briques[h][i].pos.w = wbrique;
            briques[h][i].pos.h = hbrique;
            
            if( !briques[h][i].detruit)
            SDL_RenderCopy(renderer, texture, NULL, &briques[h][i].pos); // copie dans la brique actuel dans le rendu
            
            xbrique += wbrique -6; // on augmente x pour la boucle suivante
            
        }
        Reset(); // on remet x a sa valeur d'origine pour la prochaine boucle et on descend d'un cran
        
    }
    ybrique = 30;
}

