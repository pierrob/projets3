//
//  Balle.h
//  ProjetC
//
//  Created by Pierre Bonijol on 30/11/2017.
//  Copyright © 2017 Pierre Bonijol. All rights reserved.
//

#ifndef Balle_h
#define Balle_h
#include <SDL2/SDL.h>
#include <SDL2_image/SDL_image.h>

typedef struct _Balle {
 SDL_Rect pos;
    int xvel;
    int yvel;
}Balle;

extern SDL_Surface* surfaceBalle;
extern SDL_Texture* textureBalle;

extern void MouvementBalle(void);
extern void checkb(void);

#endif /* Balle_h */
