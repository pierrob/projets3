//
//  Briques.h
//  ProjetC
//
//  Created by Pierre Bonijol on 29/11/2017.
//  Copyright © 2017 Pierre Bonijol. All rights reserved.
//
#include <SDL2/SDL.h>
#include <SDL2_image/SDL_image.h>
#ifndef Briques_h
#define Briques_h

typedef struct _Brique {    //Structure de la brique
    SDL_Rect pos; //toutes les nouvelles positions seront gérées ici
    int detruit;
    
}Brique;

extern SDL_Surface* surfaceBrique;
extern SDL_Texture* textureBrique;

extern int wbrique; //largeur de la brique
extern int hbrique;//hauteur de la brique

extern int xbrique; //abscisse de la brique
extern int ybrique; // ordonné de la brique

#endif /* Briques_h */
