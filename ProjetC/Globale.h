//
//  Globale.h
//  ProjetC
//
//  Created by Pierre Bonijol on 29/11/2017.
//  Copyright © 2017 Pierre Bonijol. All rights reserved.
//

#ifndef Globale_h
#define Globale_h

#include <stdio.h>
#include <SDL2/SDL.h>
#include <SDL2_image/SDL_image.h>
#include "Barre.h"
#include "Balle.h"
#include "Briques.h"


//Fichier qui contient toutes les definitions et references entre les headers

extern SDL_Surface* surfaceBg;

extern SDL_Window* window;

extern SDL_Renderer* renderer;
extern SDL_Texture* textureBg;

extern Barre barre;
extern Balle balle;
extern Brique **briques;

extern void GenererBrique(int longueur,int hauteur,SDL_Texture* brique);
extern void Generer(SDL_Texture* nom,int x, int y, int w, int h, SDL_Rect pos);
extern void charger_images(void);
extern void freeimages(void);
extern void charger_textures(void);
extern int initialisation(void);
extern void Update(void);
extern int collides(SDL_Rect a, SDL_Rect b);
extern int CheckCollisionBriqueBalle(SDL_Rect rect1, SDL_Rect rect2);

extern int hauteurfenetre;
extern int largeurfenetre;
extern int tir;
extern int left;
extern int right;
extern int speed;
extern int nb_brique_colonne;
extern int nb_brique_ligne;
extern int score;
extern int barreok;

#endif /* Globale_h */
